package com.street_light.classroom_manager;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.street_light.classroom_manager.auth.AuthController;
import com.street_light.classroom_manager.file.FileController;
import com.street_light.classroom_manager.generated.jooq.tables.daos.*;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Message;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.User;
import com.street_light.classroom_manager.group.GroupController;
import com.zaxxer.hikari.HikariDataSource;
import de.mkammerer.argon2.Argon2;
import de.mkammerer.argon2.Argon2Factory;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.Vertx;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.bridge.BridgeOptions;
import io.vertx.ext.bridge.PermittedOptions;
import io.vertx.ext.jwt.JWTOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.auth.jwt.JWTAuth;
import io.vertx.ext.web.handler.JWTAuthHandler;
import io.vertx.ext.web.handler.sockjs.SockJSHandler;
import org.flywaydb.core.Flyway;
import org.jooq.impl.DefaultConfiguration;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.List;


public class Server extends AbstractVerticle {

    private Gson gson = new GsonBuilder()
//                .excludeFieldsWithoutExposeAnnotation()
            .create();

    private HikariDataSource dataSource = new HikariDataSource();

    private Flyway flyway = new Flyway();

    private Argon2 argon2 = Argon2Factory.create(Argon2Factory.Argon2Types.ARGON2i);

    @Override
    public void start(Future<Void> fut)  {



        dataSource.setDataSourceClassName("org.postgresql.ds.PGSimpleDataSource");
        dataSource.addDataSourceProperty("serverName", "localhost");
        dataSource.addDataSourceProperty("databaseName", "classroom_manager");
        dataSource.addDataSourceProperty("user", "postgres");
        dataSource.addDataSourceProperty("password", "local");


        flyway.setDataSource(dataSource);
        flyway.baseline();
        flyway.migrate();


        System.out.println(argon2.hash(32, 16384, 1, "password"));

        UserDao userDao = new UserDao(new DefaultConfiguration()
                .set(dataSource));
        GroupDao groupDao = new GroupDao(new DefaultConfiguration()
                .set(dataSource));
        GroupUserDao groupUserDao = new GroupUserDao(new DefaultConfiguration()
                .set(dataSource));
        MessageDao messageDao = new MessageDao(new DefaultConfiguration()
                .set(dataSource));
        FileDao fileDao = new FileDao(new DefaultConfiguration()
                .set(dataSource));
        GroupFileDao groupFileDao = new GroupFileDao(new DefaultConfiguration()
                .set(dataSource));

        AuthController authController = new AuthController(userDao, argon2, vertx, gson);
        GroupController groupController = new GroupController(groupDao, userDao, groupUserDao, gson, vertx);
        FileController fileController = new FileController(fileDao, groupDao, groupFileDao, gson);

        Router router = Router.router(vertx);

        router.route("/api/*").handler(JWTAuthHandler.create(authController.getJwt()));
        router.route("/api/*").handler(BodyHandler.create());
        router.route("/auth/*").handler(BodyHandler.create());
        router.post("/auth/login").handler(authController::LogIn);
        router.post("/auth/register").handler(authController::Register);
        //hook group methods
        router.get("/api/:user/groups").handler(groupController::fetchGroupsByUser);
        router.post("/api/group").handler(groupController::createGroup);
        router.put("/api/update_group").handler(groupController::updateGroup);
        router.delete("/api/group/:id").handler(groupController::deleteGroup);
        router.get("/api/group/:id").handler(groupController::fetchGroup);
        //hook file methods
        router.post("/api/file").handler(fileController::createFile);
        router.get("/api/file/:id").handler(fileController::fetchFile);
        router.delete("/api/file/:id").handler(fileController::deleteFile);

        HttpServer httpServer = vertx
                .createHttpServer()
                .requestHandler(router::accept);

        BridgeOptions opts = new BridgeOptions()
                .addInboundPermitted(new PermittedOptions().setAddress("chat.to.server"))
                .addOutboundPermitted(new PermittedOptions().setAddress("chat.to.client"));

        SockJSHandler ebHandler = SockJSHandler.create(vertx);
        router.route("/eventbus/*").handler(ebHandler);

        EventBus eb = vertx.eventBus();

        httpServer.listen(4567);

        eb.consumer("chat.to.server").handler(message->{
            MessageForm messageForm = gson.fromJson(message.body().toString(), MessageForm.class);

            Message _message = new Message();
            _message.setText(messageForm.getMessage());
            _message.setChatUuid(messageForm.groupId);
            _message.setTime(Timestamp.from(Instant.now()));

            messageDao.insert(_message);

            eb.publish("chat.to.client." + _message.getChatUuid(), gson.toJson(_message));
        });
    }
}