package com.street_light.classroom_manager.file.forms;

public class FileForm {
    String file;
    String name;
    int groupId;

    public FileForm(String file, String name, int groupId) {
        this.file = file;
        this.name = name;
        this.groupId = groupId;
    }

    public String getFile() {
        return file;
    }

    public int getGroupId() {
        return groupId;
    }

    public String getName() {
        return name;
    }
}
