package com.street_light.classroom_manager.file;

import com.google.gson.Gson;
import com.street_light.classroom_manager.auth.forms.ResponseForm;
import com.street_light.classroom_manager.file.forms.FileForm;
import com.street_light.classroom_manager.generated.jooq.tables.daos.FileDao;
import com.street_light.classroom_manager.generated.jooq.tables.daos.GroupDao;
import com.street_light.classroom_manager.generated.jooq.tables.daos.GroupFileDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.File;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.GroupFile;
import io.vertx.ext.web.RoutingContext;

import java.util.List;

public class FileController {
    FileDao fileDao;
    GroupDao groupDao;
    GroupFileDao groupFileDao;
    Gson gson;

    public FileController(FileDao fileDao, GroupDao groupDao, GroupFileDao groupFileDao, Gson gson) {
        this.fileDao = fileDao;
        this.groupDao = groupDao;
        this.groupFileDao = groupFileDao;
        this.gson = gson;
    }

    public void createFile(RoutingContext ctx){
        FileForm fileForm = gson.fromJson(ctx.getBodyAsString(), FileForm.class);

        File file = new File();
        file.setFile(fileForm.getFile().getBytes());
        file.setName(file.getName());

        fileDao.insert(file);

        List<File> files = fileDao.fetchByFile(fileForm.getFile().getBytes());

        GroupFile groupFile = new GroupFile();
        groupFile.setFileId(files.get(0).getId());
        groupFile.setGroupId(fileForm.getGroupId());

        ResponseForm res = new ResponseForm(false, "", -1);

        res.setSuccess(true);
        res.setMessage(files.get(0).getId().toString());
        res.setId(files.get(0).getId());

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(res));
    }

    public void fetchFile(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        File file = fileDao.fetchOneById(id);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(file));
    }

    public void deleteFile(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        File file = fileDao.fetchOneById(id);

        fileDao.delete(file);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end("OK");
    }
}
