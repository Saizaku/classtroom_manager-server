/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq.tables.pojos;


import java.io.Serializable;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Links implements Serializable {

    private static final long serialVersionUID = -702247941;

    private String link;

    public Links() {}

    public Links(Links value) {
        this.link = value.link;
    }

    public Links(
        String link
    ) {
        this.link = link;
    }

    public String getLink() {
        return this.link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Links (");

        sb.append(link);

        sb.append(")");
        return sb.toString();
    }
}
