/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq;


import com.street_light.classroom_manager.generated.jooq.tables.File;
import com.street_light.classroom_manager.generated.jooq.tables.Group;
import com.street_light.classroom_manager.generated.jooq.tables.GroupFile;
import com.street_light.classroom_manager.generated.jooq.tables.GroupLink;
import com.street_light.classroom_manager.generated.jooq.tables.GroupUser;
import com.street_light.classroom_manager.generated.jooq.tables.Links;
import com.street_light.classroom_manager.generated.jooq.tables.Message;
import com.street_light.classroom_manager.generated.jooq.tables.SchemaVersion;
import com.street_light.classroom_manager.generated.jooq.tables.User;

import javax.annotation.Generated;

import org.jooq.Index;
import org.jooq.OrderField;
import org.jooq.impl.Internal;


/**
 * A class modelling indexes of tables of the <code>public</code> schema.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Indexes {

    // -------------------------------------------------------------------------
    // INDEX definitions
    // -------------------------------------------------------------------------

    public static final Index FILE_PKEY = Indexes0.FILE_PKEY;
    public static final Index GROUP_PKEY = Indexes0.GROUP_PKEY;
    public static final Index GROUP_FILE_PKEY = Indexes0.GROUP_FILE_PKEY;
    public static final Index GROUP_LINK_PKEY = Indexes0.GROUP_LINK_PKEY;
    public static final Index GROUP_USER_PKEY = Indexes0.GROUP_USER_PKEY;
    public static final Index LINKS_PKEY = Indexes0.LINKS_PKEY;
    public static final Index MESSAGE_PKEY = Indexes0.MESSAGE_PKEY;
    public static final Index SCHEMA_VERSION_PK = Indexes0.SCHEMA_VERSION_PK;
    public static final Index SCHEMA_VERSION_S_IDX = Indexes0.SCHEMA_VERSION_S_IDX;
    public static final Index USER_PKEY = Indexes0.USER_PKEY;

    // -------------------------------------------------------------------------
    // [#1459] distribute members to avoid static initialisers > 64kb
    // -------------------------------------------------------------------------

    private static class Indexes0 {
        public static Index FILE_PKEY = Internal.createIndex("file_pkey", File.FILE, new OrderField[] { File.FILE.ID }, true);
        public static Index GROUP_PKEY = Internal.createIndex("group_pkey", Group.GROUP, new OrderField[] { Group.GROUP.ID }, true);
        public static Index GROUP_FILE_PKEY = Internal.createIndex("group_file_pkey", GroupFile.GROUP_FILE, new OrderField[] { GroupFile.GROUP_FILE.ID }, true);
        public static Index GROUP_LINK_PKEY = Internal.createIndex("group_link_pkey", GroupLink.GROUP_LINK, new OrderField[] { GroupLink.GROUP_LINK.ID }, true);
        public static Index GROUP_USER_PKEY = Internal.createIndex("group_user_pkey", GroupUser.GROUP_USER, new OrderField[] { GroupUser.GROUP_USER.ID }, true);
        public static Index LINKS_PKEY = Internal.createIndex("links_pkey", Links.LINKS, new OrderField[] { Links.LINKS.LINK }, true);
        public static Index MESSAGE_PKEY = Internal.createIndex("message_pkey", Message.MESSAGE, new OrderField[] { Message.MESSAGE.ID }, true);
        public static Index SCHEMA_VERSION_PK = Internal.createIndex("schema_version_pk", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.INSTALLED_RANK }, true);
        public static Index SCHEMA_VERSION_S_IDX = Internal.createIndex("schema_version_s_idx", SchemaVersion.SCHEMA_VERSION, new OrderField[] { SchemaVersion.SCHEMA_VERSION.SUCCESS }, false);
        public static Index USER_PKEY = Internal.createIndex("user_pkey", User.USER, new OrderField[] { User.USER.ID }, true);
    }
}
