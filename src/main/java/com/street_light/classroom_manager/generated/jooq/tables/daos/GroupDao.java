/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq.tables.daos;


import com.street_light.classroom_manager.generated.jooq.tables.Group;
import com.street_light.classroom_manager.generated.jooq.tables.records.GroupRecord;

import java.util.List;

import javax.annotation.Generated;

import org.jooq.Configuration;
import org.jooq.impl.DAOImpl;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class GroupDao extends DAOImpl<GroupRecord, com.street_light.classroom_manager.generated.jooq.tables.pojos.Group, Integer> {

    /**
     * Create a new GroupDao without any configuration
     */
    public GroupDao() {
        super(Group.GROUP, com.street_light.classroom_manager.generated.jooq.tables.pojos.Group.class);
    }

    /**
     * Create a new GroupDao with an attached configuration
     */
    public GroupDao(Configuration configuration) {
        super(Group.GROUP, com.street_light.classroom_manager.generated.jooq.tables.pojos.Group.class, configuration);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Integer getId(com.street_light.classroom_manager.generated.jooq.tables.pojos.Group object) {
        return object.getId();
    }

    /**
     * Fetch records that have <code>id IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchById(Integer... values) {
        return fetch(Group.GROUP.ID, values);
    }

    /**
     * Fetch a unique record that has <code>id = value</code>
     */
    public com.street_light.classroom_manager.generated.jooq.tables.pojos.Group fetchOneById(Integer value) {
        return fetchOne(Group.GROUP.ID, value);
    }

    /**
     * Fetch records that have <code>name IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchByName(String... values) {
        return fetch(Group.GROUP.NAME, values);
    }

    /**
     * Fetch records that have <code>color IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchByColor(String... values) {
        return fetch(Group.GROUP.COLOR, values);
    }

    /**
     * Fetch records that have <code>raspored IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchByRaspored(Integer... values) {
        return fetch(Group.GROUP.RASPORED, values);
    }

    /**
     * Fetch records that have <code>zvona IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchByZvona(Integer... values) {
        return fetch(Group.GROUP.ZVONA, values);
    }

    /**
     * Fetch records that have <code>image IN (values)</code>
     */
    public List<com.street_light.classroom_manager.generated.jooq.tables.pojos.Group> fetchByImage(Integer... values) {
        return fetch(Group.GROUP.IMAGE, values);
    }
}
