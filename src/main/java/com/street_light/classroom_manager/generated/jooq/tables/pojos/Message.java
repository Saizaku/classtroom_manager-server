/*
 * This file is generated by jOOQ.
 */
package com.street_light.classroom_manager.generated.jooq.tables.pojos;


import java.io.Serializable;
import java.sql.Timestamp;

import javax.annotation.Generated;


/**
 * This class is generated by jOOQ.
 */
@Generated(
    value = {
        "http://www.jooq.org",
        "jOOQ version:3.11.7"
    },
    comments = "This class is generated by jOOQ"
)
@SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Message implements Serializable {

    private static final long serialVersionUID = -2087121163;

    private Integer   id;
    private String    text;
    private Integer   userId;
    private Integer   chatUuid;
    private Timestamp time;

    public Message() {}

    public Message(Message value) {
        this.id = value.id;
        this.text = value.text;
        this.userId = value.userId;
        this.chatUuid = value.chatUuid;
        this.time = value.time;
    }

    public Message(
        Integer   id,
        String    text,
        Integer   userId,
        Integer   chatUuid,
        Timestamp time
    ) {
        this.id = id;
        this.text = text;
        this.userId = userId;
        this.chatUuid = chatUuid;
        this.time = time;
    }

    public Integer getId() {
        return this.id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return this.text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getChatUuid() {
        return this.chatUuid;
    }

    public void setChatUuid(Integer chatUuid) {
        this.chatUuid = chatUuid;
    }

    public Timestamp getTime() {
        return this.time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Message (");

        sb.append(id);
        sb.append(", ").append(text);
        sb.append(", ").append(userId);
        sb.append(", ").append(chatUuid);
        sb.append(", ").append(time);

        sb.append(")");
        return sb.toString();
    }
}
