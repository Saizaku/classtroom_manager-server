package com.street_light.classroom_manager;

import java.sql.Timestamp;

public class MessageForm {
    String message;
    int userId;
    int groupId;

    public MessageForm(String message, int userId, int groupId) {
        this.message = message;
        this.userId = userId;
        this.groupId = groupId;
    }

    Timestamp timeStamp;

    public String getMessage() {
        return message;
    }

    public int getUserId() {
        return userId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public void setTimeStamp(Timestamp timeStamp) {
        this.timeStamp = timeStamp;
    }
}
