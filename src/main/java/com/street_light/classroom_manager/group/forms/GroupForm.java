package com.street_light.classroom_manager.group.forms;

public class GroupForm {
    String name;
    String color;
    int userID;

    public Integer getUserID() {
        return userID;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }


    public GroupForm(String name, String color, int userID){
        this.name = name;
        this.color = color;
        this.userID = userID;
    }
}
