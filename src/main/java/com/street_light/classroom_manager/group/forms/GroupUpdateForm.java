package com.street_light.classroom_manager.group.forms;

public class GroupUpdateForm {
    int id;
    String name;
    String color;
    int image;
    int raspored;
    int zvona;

    public GroupUpdateForm(int id, String name, String color, int image, int raspored, int zvona) {
        this.id = id;
        this.name = name;
        this.color = color;
        this.image = image;
        this.raspored = raspored;
        this.zvona = zvona;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    public int getImage() {
        return image;
    }

    public int getRaspored() {
        return raspored;
    }

    public int getZvona() {
        return zvona;
    }
}
