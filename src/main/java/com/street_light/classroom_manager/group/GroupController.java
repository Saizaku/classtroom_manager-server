package com.street_light.classroom_manager.group;

import com.google.gson.Gson;
import com.street_light.classroom_manager.generated.jooq.tables.daos.GroupDao;
import com.street_light.classroom_manager.generated.jooq.tables.daos.GroupUserDao;
import com.street_light.classroom_manager.generated.jooq.tables.daos.UserDao;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.Group;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.GroupUser;
import com.street_light.classroom_manager.generated.jooq.tables.pojos.User;
import com.street_light.classroom_manager.group.forms.GroupForm;
import com.street_light.classroom_manager.group.forms.GroupUpdateForm;
import io.vertx.core.Vertx;
import io.vertx.ext.web.RoutingContext;
import org.jooq.meta.derby.sys.Sys;


import java.util.ArrayList;
import java.util.List;

public class GroupController {
    GroupDao groupDao;
    UserDao userDao;
    GroupUserDao groupUserDao;
    Vertx vertx;
    Gson gson;

    public GroupController(GroupDao groupDao, UserDao userDao, GroupUserDao groupUserDao, Gson gson, Vertx vertx){
        this.groupDao = groupDao;
        this.userDao = userDao;
        this.groupUserDao = groupUserDao;
        this.gson = gson;
        this.vertx = vertx;
    }

    public void fetchGroupsByUser(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("user"));

        User user = userDao.fetchOneById(id);
        if(user == null){
            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end();
            return;
        }

        List<GroupUser> groupUsers = groupUserDao.fetchByUserId(user.getId());

        if(groupUsers.isEmpty()){
            ctx.response()
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .end();
            return;
        }

        List<Group> groups = new ArrayList<Group>();

        for(int i = 0; i < groupUsers.size(); i++){
            groups.add(groupDao.fetchOneById(groupUsers.get(i).getGroupId()));
        }


        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(groups));
    }

    public void createGroup(RoutingContext ctx){



        GroupForm groupForm = gson.fromJson(ctx.getBodyAsString(), GroupForm.class);

        Group group = new Group();
        group.setName(groupForm.getName());
        group.setColor(groupForm.getColor());

        groupDao.insert(group);

        GroupUser groupUser = new GroupUser();
        groupUser.setColor("777777");
        List<Group> groups = groupDao.findAll();
        int groupID = groups.get(groups.size()-1).getId();
        groupUser.setGroupId(groupID);
        groupUser.setUserId(groupForm.getUserID());

        groupUserDao.insert(groupUser);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(group));
    }

    public void fetchGroup(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        Group group = groupDao.fetchOneById(id);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(group));
    }

    public void updateGroup(RoutingContext ctx){
        GroupUpdateForm groupForm = gson.fromJson(ctx.getBodyAsString(), GroupUpdateForm.class);

        Group group = groupDao.fetchOneById(groupForm.getId());
        group.setName(groupForm.getName());
        group.setColor(groupForm.getColor());
        group.setImage(groupForm.getImage());
        group.setRaspored(groupForm.getRaspored());
        group.setZvona(groupForm.getZvona());

        groupDao.update(group);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(gson.toJson(group));
    }

    public void deleteGroup(RoutingContext ctx){
        int id = Integer.parseInt(ctx.request().getParam("id"));

        Group group = groupDao.fetchOneById(id);
        groupDao.delete(group);

        ctx.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end("OK");
    }
}
