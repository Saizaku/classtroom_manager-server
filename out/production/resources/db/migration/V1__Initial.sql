CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    username character varying(128) COLLATE pg_catalog."default" NOT NULL,
    email character varying(128) COLLATE pg_catalog."default" NOT NULL,
    password character varying(512) COLLATE pg_catalog."default",
    date_created timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    image integer,
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT image_fk FOREIGN KEY (image)
        REFERENCES public.file (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.file
(
    id integer NOT NULL DEFAULT nextval('file_id_seq'::regclass),
    file bytea,
    CONSTRAINT file_pkey PRIMARY KEY (id)
);

CREATE TABLE public."group"
(
    id integer NOT NULL DEFAULT nextval('group_id_seq'::regclass),
    name character varying(64) COLLATE pg_catalog."default",
    color character varying(6) COLLATE pg_catalog."default",
    raspored integer,
    zvona integer,
    image integer,
    CONSTRAINT group_pkey PRIMARY KEY (id),
    CONSTRAINT image_fk FOREIGN KEY (image)
        REFERENCES public.file (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT raspored_fk FOREIGN KEY (raspored)
        REFERENCES public.file (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT zvona_fk FOREIGN KEY (zvona)
        REFERENCES public.file (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.links
(
    link character varying(512) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT links_pkey PRIMARY KEY (link)
);

CREATE TABLE public.group_link
(
    id integer NOT NULL DEFAULT nextval('group_link_id_seq'::regclass),
    group_id integer NOT NULL,
    link_id character varying(512) COLLATE pg_catalog."default" NOT NULL,
    CONSTRAINT group_link_pkey PRIMARY KEY (id),
    CONSTRAINT group_id_fk FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT link_id_fk FOREIGN KEY (link_id)
        REFERENCES public.links (link) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.group_user
(
    id integer NOT NULL DEFAULT nextval('group_user_id_seq'::regclass),
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    nick character varying(32) COLLATE pg_catalog."default",
    color character varying(6) COLLATE pg_catalog."default",
    moderator boolean DEFAULT false,
    CONSTRAINT group_user_pkey PRIMARY KEY (id),
    CONSTRAINT group_id_fk FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.message
(
    id integer NOT NULL DEFAULT nextval('message_id_seq'::regclass),
    text character varying(512) COLLATE pg_catalog."default",
    user_id integer NOT NULL,
    chat_uuid integer NOT NULL,
    "time" timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT message_pkey PRIMARY KEY (id),
    CONSTRAINT user_id_fk FOREIGN KEY (user_id)
        REFERENCES public."user" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);

CREATE TABLE public.group_file
(
    id integer NOT NULL DEFAULT nextval('group_file_id_seq'::regclass),
    group_id integer,
    file_id integer,
    CONSTRAINT group_file_pkey PRIMARY KEY (id),
    CONSTRAINT file_id_fk FOREIGN KEY (file_id)
        REFERENCES public.file (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT group_id_fk FOREIGN KEY (group_id)
        REFERENCES public."group" (id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION
);